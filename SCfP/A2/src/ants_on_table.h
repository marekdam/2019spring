// ants_on_table.h
// Damian Marek - 24/01/2019
#ifndef ANTS_ON_TABLE_H
#define ANTS_ON_TABLE_H
// Handles all data that describes the ants on the table
// has functions for initializing, placing ants and destroying

struct ants_on_table
{

    // Allocate memory and setup data
    void initialize(int length, int total_ants);
    // Place the ants evenly on the table
    void place_ants_start();
    // Releases memory, use when finished with ants
    void destroy();

    const int nmoves = 9; // There are (3 in the i direction)x(3 in the j direction)=9 possible moves
    int *imoves;          // = {-1, -1, -1, 0, 0, 0, 1, 1, 1}; // Effect of each move on the i direction
    int *jmoves;          // = {-1, 0, 1, -1, 0, 1, -1, 0, 1}; // Effect of each move on the j direction

    // parameters
    int length;     // length of the table
    int total_ants; // initial number of ants

    // work arrays
    int *number_of_ants;     // distribution of ants on the table over squares.
    int *new_number_of_ants; // auxiliary array used in time step to hold the new distribution of ants
    int *partition;          // used to determine how many ants move in which direction in a time step
    int nmin;                // will hold the minimum number of ants on any square
    int nmax;                // will hold the maximum number of ants on any square
};

#endif /* ANTS_ON_TABLE_H */
