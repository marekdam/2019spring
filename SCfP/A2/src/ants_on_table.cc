#include "ants_on_table.h"

void ants_on_table::initialize(int length, int total_ants)
{
    this->length = length;         // length of the table
    this->total_ants = total_ants; // initial number of ants

    this->nmin = total_ants; // will hold the minimum number of ants on any square
    this->nmax = 0;          // will hold the maximum number of ants on any square

    number_of_ants = new int[length * length];
    new_number_of_ants = new int[length * length];

    //Allocate memory
    //The simulation is allowed to fail in case of an exception

    //These arrays could probably be automatic since they are small
    //however as per the assignment I made them dynamic.
    partition = new int[nmoves];
    imoves = new int[nmoves];
    jmoves = new int[nmoves];

    //Populate move arrays
    // = {-1, -1, -1, 0, 0, 0, 1, 1, 1}; i direction
    // = {-1, 0, 1, -1, 0, 1, -1, 0, 1}; j direction
    // after these are set they should never be changed
    for (int i = 0; i < nmoves; i++)
    {
        imoves[i] = -1 + i / 3;
        jmoves[i] = -1 + i % 3;
    }
}

// place the ants evenly on the table
void ants_on_table::place_ants_start()
{
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            int n = i * length + j; // linear index
            number_of_ants[n] = ((long long)(n + 1) * total_ants) / (length * length) - ((long long)(n)*total_ants) / (length * length);
        }
    }
}

void ants_on_table::destroy()
{
    delete[] number_of_ants;
    delete[] new_number_of_ants;
    delete[] partition;
    delete[] imoves;
    delete[] jmoves;
}
