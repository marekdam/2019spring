// ants_simulator.h
// Damian Marek - 24/01/2019
#ifndef ANTS_SIMULATOR_H
#define ANTS_SIMULATOR_H
#include <cstddef> //need size_t type
// Only need to forward declare, since only need the reference.
struct ants_on_table;

// Function which updates the nmin and nmax values of
// ants on table
void count_ants(ants_on_table &ants_on_table);

// Main body of simulation performs one time step
//seed is the value used in the random number generator
void calc_one_time_step(ants_on_table &ants_on_table, const size_t seed);

#endif /* ANTS_SIMULATOR_H */