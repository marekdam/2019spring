// ants_report.h
// Damian Marek - 24/01/2019
#ifndef ANTS_REPORT_H
#define ANTS_REPORT_H

//Only need to forward declare, since only need the reference.
struct ants_on_table;

// Given struct that represents the current state of the ants
// create a small message to be sent to std::cout
void report_ants_status(int time_step, const ants_on_table &ants_on_table);

#endif /* ANTS_REPORT_H */