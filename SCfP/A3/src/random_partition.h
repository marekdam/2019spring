// random_partition.h
// Damian Marek - 31/01/2019
#ifndef RANDOM_PARTITION_H
#define RANDOM_PARTITION_H
#include "rarray"
#include <cstddef> //needed for size_t type
// Function to randomly divide a number 'total' into a set of
// 'numdivision' numbers that add up to 'total'.
//
// Parameters:
//   total     number that is to be subdivided (input)
//   nparts    number of partitions into which to divide 'total (input)
//   nperpart  resulting number in each partition (output)
//   seed      the seed for the random number generator (input)
//
void rand_partition(int total, int nparts, rarray<int, 1> &nperpart, size_t seed);

#endif /* RANDOM_PARTITION_H */
