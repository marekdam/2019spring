#include "ants_io.h"
#include "ants_on_table.h"
#include <iostream>

using namespace netCDF;

// Given an struct of ants_on_table creates the correct values for the netCDF file and dimensions
void ants_archiver::initialize(const ants_on_table &ants_on_table, const std::string &filename)
{
    using namespace netCDF;
    // Setup file that will store ants data
    ants_file.open(filename, NcFile::replace);
    ants_file.putAtt("Summary:",
                     "Stores number of ants at each grid point at different time steps");

    // Storing two variables
    const std::string ants_on_grid_name = "ants_on_grid";
    // This variable stores the current timestep number that
    // resulted in the current ant positions
    const std::string timestep_name = "timestep_number";

    int i_length = ants_on_table.number_of_ants.extent(0);
    int j_length = ants_on_table.number_of_ants.extent(1);
    // This is an infinite type dimension that can be appended to
    NcDim rDim = ants_file.addDim("records");
    // The grid of ants does not change from timestep to timestep
    NcDim iDim = ants_file.addDim("i", i_length);
    NcDim jDim = ants_file.addDim("j", j_length);

    // Create the two variables
    std::vector<NcDim> dims = {rDim, iDim, jDim};
    ants_on_grid = ants_file.addVar(ants_on_grid_name, ncInt, dims);
    std::vector<NcDim> dim_time = {rDim};
    timestep_number = ants_file.addVar(timestep_name, ncInt, dim_time);
}

// Save current positions to file and increase count of number of records
void ants_archiver::add_another_record(int current_timestep, const ants_on_table &ants_on_table)
{
    int i_length = ants_on_table.number_of_ants.extent(0);
    int j_length = ants_on_table.number_of_ants.extent(1);

    std::vector<size_t> corner_start = {(size_t)number_of_records, 0, 0};
    std::vector<size_t> size_of_data = {1, (size_t)i_length, (size_t)j_length};

    ants_on_grid.putVar(corner_start, size_of_data, ants_on_table.number_of_ants.data());

    std::vector<size_t> time_corner = {(size_t)number_of_records};
    timestep_number.putVar(time_corner, &current_timestep);
    number_of_records++;
}
