// ants_driver.cc
//
// Simulation of ants walking on a table. The table is subdivided into
// squares. A number of ants is initially put on the table, divided as
// evenly as possible over the squares.  Ants can move to a
// neighbouring square or stay put at every time step.  How many of a
// square's ants move in each of the 9 different directions is random
// (using c++11's random library).  Ants can fall of the edges of the
// table. The program prints the number of ants left on the table
// after every time step, as well as the minimum number of ants on a
// square and the maximum.
//
// Compile with:
//
//     make or make all
//
// Run with
//
//     ./antsontable > output.dat
//
// This saves the output to the file output.dat instead of writing it to
// screen.
//
// Damian Marek - changes
// January 2019
// Ramses van Zon, SciNet, University of Toronto
// January 2019

#include "ants_io.h"
#include "ants_on_table.h"
#include "ants_report.h"
#include "ants_simulator.h"

// Driver function of antsontable
int main()
{
	// simulation parameters should not be changed by later functions
	const int length = 70;		  // length of the table
	const int time_steps = 10000; // number of time steps to take
	const int total_ants = 40000; // initial number of ants
	const size_t seed = 11;		  // seed for random number generation
	// The ants are recorded at some timesteps in this binary file
	const std::string archive_filename = "ant_positions.nc";

	ants_on_table ants_on_table;
	// allocate memory
	ants_on_table.initialize(length, total_ants);
	// distribute ants onto the grid
	ants_on_table.place_ants_start();

	// saves ants data to a netCDF filetype
	ants_archiver ants_archiver;
	ants_archiver.initialize(ants_on_table, archive_filename);
	// Record initial state
	ants_archiver.add_another_record(0, ants_on_table);

	// first nmin and nmax calculation
	count_ants(ants_on_table);
	// first results reported
	report_ants_status(0, ants_on_table);

	// run time steps
	for (int t = 0; t < time_steps; t++)
	{
		calc_one_time_step(ants_on_table, seed);

		// Only save to file at timesteps that are multiples
	    // of 1000. There should be 11 in total including the zeroth.
		if (t % 1000 == 999)
		{
			// Current timestep is one after t, because
			// calc function was called
			ants_archiver.add_another_record(t + 1, ants_on_table);
		}

		count_ants(ants_on_table);

		report_ants_status(t + 1, ants_on_table);
	}

	return 0;
}
