#include "ants_report.h"
#include "ants_on_table.h"
#include <iostream>

// Given struct that represents the current state of the ants
// create a small message to be sent to std::cout
void report_ants_status(int time_step, const ants_on_table &ants_on_table)
{
    const int total_ants = ants_on_table.total_ants;
    const int nmin = ants_on_table.nmin;
    const int nmax = ants_on_table.nmax;
    // report
    std::cout << time_step << " " << total_ants << " " << nmin << " " << nmax << std::endl;
}
