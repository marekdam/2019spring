#include "ants_simulator.h"
#include "ants_on_table.h"
#include "random_partition.h"

// Function which updates the nmin and nmax values of
// ants on table
void count_ants(ants_on_table &ants_on_table)
{
    // Get data from struct
    // was easier than modifying the code below
    // by renaming all of the variables
    const rarray<int, 2> &number_of_ants = ants_on_table.number_of_ants;
    const int length = ants_on_table.number_of_ants.extent(0);
    int &nmin = ants_on_table.nmin;
    int &nmax = ants_on_table.nmax;
    int &total_ants = ants_on_table.total_ants;

    // count ants and determine minimum and maximum number on a square
    nmin = total_ants;
    nmax = 0;
    total_ants = 0;
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            total_ants += number_of_ants[i][j];
            if (nmin > number_of_ants[i][j])
            {
                nmin = number_of_ants[i][j];
            }
            if (nmax < number_of_ants[i][j])
            {
                nmax = number_of_ants[i][j];
            }
        }
    }
}

// Main body of simulation loop performs one time step
void calc_one_time_step(ants_on_table &ants_on_table, const size_t seed)
{
    // Get data from struct
    // was easier than modifying the code below
    // renaming all of the variables
    rarray<int, 2> &number_of_ants = ants_on_table.number_of_ants;
    rarray<int, 2> &new_number_of_ants = ants_on_table.new_number_of_ants;

    const int length = number_of_ants.extent(0);

    rarray<int, 1> &partition = ants_on_table.partition;
    const int nmoves = ants_on_table.nmoves;
    const rarray<int, 1> &imoves = ants_on_table.imoves;
    const rarray<int, 1> &jmoves = ants_on_table.jmoves;

    // ants move to a new an auxiliary new 'table', empty this one first
    new_number_of_ants.fill(0);

    // now move ants into auxiliary table
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            if (number_of_ants[i][j] > 0)
            {
                // pick how many ants go in each of the 9 moves
                rand_partition(number_of_ants[i][j], nmoves, partition, seed);
                // push ants in their respective moves
                for (int m = 0; m < nmoves; m++)
                {
                    int i2 = i + imoves[m];
                    int j2 = j + jmoves[m];
                    // put only on new table if not falling off table
                    if (i2 >= 0 and i2 < length and j2 >= 0 and j2 < length)
                    {
                        new_number_of_ants[i2][j2] += partition[m];
                    }
                }
            }
        }
    }

    number_of_ants = new_number_of_ants.copy();
}
