// ants_io.h
// Damian Marek - 31/01/2019
#ifndef ANTS_IO_H
#define ANTS_IO_H
#include "rarray"
#include <netcdf>

//Only need to forward declare, since only need the reference.
struct ants_on_table;

struct ants_archiver
{
    netCDF::NcFile ants_file;
    netCDF::NcVar ants_on_grid;
    netCDF::NcVar timestep_number;
    int number_of_records = 0;

    void initialize(const ants_on_table &ants_on_table, const std::string &filename);

    void add_another_record(int current_timestep, const ants_on_table &ants_on_table);
};

#endif /* ANTS_IO_H */
