# !/bin/bash

#Make subjobs for gnu parallel
rm -f subjobs.txt
n=1;
max=200;
while [ "$n" -le "$max" ]; do
	cat <<EOM >>subjobs.txt
	cd jobdir$n; ../../antsontable/antsontable params.ini; echo "job $n done"
EOM
	n=`expr "$n" + 1`;
done

# Make all 200 directories that will hold output of data and input parameters
n=1;
max=200;
while [ "$n" -le "$max" ]; do
	mkdir -p "jobdir$n"
	n=`expr "$n" + 1`;
done

n=1;
length=100;
l_max=300;

#Populate the ini files with different parameters
while [ "$length" -le "$l_max" ]; do
	seed=1;
	s_max=40;
	while [ "$seed" -le "$s_max" ]; do		
		cd "jobdir$n"

		#Command makes files with different lengths and seeds
		touch params.ini

		cat <<EOM >params.ini
length=$length
seed=$seed
total_ants=20000
time_steps=100000
screen_output_steps=0
netcdf_output_steps=0
EOM
		
		cd ..
		
		seed=`expr "$seed" + 1`;
		n=`expr "$n" + 1`;
	done
	length=`expr "$length" + 50`;
done
