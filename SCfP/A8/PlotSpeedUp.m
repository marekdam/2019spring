clear;
close all;
%Number of processors
np = 1:16;
%Runtime in Hours:Minutes:Seconds
runtime_str = [ '02:52:28'; '01:26:16'; '00:58:15'; '00:43:46'; '00:35:32'; '00:31:05'; '00:27:57'; '00:24:13'; ...
	'00:22:07'; '00:19:22'; '00:18:25'; '00:17:01'; '00:16:33';'00:14:58'; '00:14:06'; '00:13:14']; 

% initialize variables
NUM_SECONDS_PER_DAY = 86400.0;

% convert times to fractional days using datenum
timeFractionalDays = datenum(runtime_str);
% leave only the part with the most recent day fraction
timeDayFraction = mod(timeFractionalDays,1);
% multiply by number of seconds in a day
rt_secs = (timeDayFraction .* NUM_SECONDS_PER_DAY)';

fileID = fopen('scaling.dat','w');
fprintf(fileID,'%d \n', rt_secs);
fclose(fileID);

figure(1)
plot(np, rt_secs(1)./rt_secs, np, np);
legend('Measured Speedup', 'Ideal', 'Location', 'SouthEast');
title('Scaling Analysis of GNU Parallel Jobs')
ylabel('Speedup');
xlabel('Number of processors');