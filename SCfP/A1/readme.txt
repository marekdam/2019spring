Damian Marek - 17/01/2019

This c++ file (produce_table_data.cpp) calculates the value of sin2x and cos3x on 101 equidistant points in the range [-5.0, 5.0] and saves the data to a file named "lissajous.txt".

To compile the program use the following command:
   g++ -std=c++11 produce_table_data.cpp -o produce_table_data
To run the resulting program use:
   ./produce_table_data
