// produce_table_data.cpp
// Damian Marek
// 17/01/2019
#include <cmath>
#include <fstream>

//returns sin2x with x in radians
double f(double x)
{
	return sin(2*x);
}

//returns cos3x with x in radians
double g(double x)
{
	return cos(3*x);
}

//Produces text file containing data of sin2x and cos3x
//on 101 equidistant points over a domain of [-5.0, 5.0].
int main()
{
	//Assigning variables for later use
	const std::string filename  = "lissajous.txt";
	const double x_min = -5.0;
	const double x_max = 5.0;
	const int num_points = 101;

	//Object managing file operations
	std::ofstream file_stream;
	file_stream.open(filename);

	//Check if file was opened successfully
	if(file_stream.is_open()){
		//calculate distance between points
		const double dx = (x_max - x_min)/(num_points - 1);
		
		//names of each column "x |  f(x) |  g(x)"
		file_stream << "x \t\tf(x) \t\tg(x)" << "\n" << std::fixed;
		//helps keep columns same size by fixing precision
		
		for(int i = 0; i < num_points; i++){
			double x = x_min+i*dx;//x position update
			//calculate values with functions and send to stream
			file_stream << x << "  \t" << f(x) << "  \t" << g(x) << "\n";
		}
		file_stream.close();
	}else{
		//failed to open, return with error
		return -1;
	}
  
	return 0;
}
